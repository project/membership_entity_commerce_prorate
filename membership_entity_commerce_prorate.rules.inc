<?php

/**
 * @file
 * Rules condition and action callbacks.
 */

/**
 * Implements hook_rules_condition_info().
 */
function membership_entity_commerce_prorate_rules_condition_info() {
  return array(
    'membership_entity_commerce_prorate_condition_current_membership' => array(
      'label' => t('User has current membership'),
      'arguments' => array(
        'user' => array(
          'type' => 'user',
          'label' => t('User'),
        ),
      ),
      'module' => 'membership_entity_commerce_prorate',
      'group' => t('Prorate Conditions'),
    ),
  );
}

/**
 * Implements hook_rules_action_info().
 */
function membership_entity_commerce_prorate_rules_action_info() {
  $actions = array();
  $actions['membership_entity_commerce_prorate_apply_prorating_discount'] = array(
    'label' => t('Calculate prorating percentage'),
    'group' => 'Prorate Actions',
    'callbacks' => array(
      'execute' => 'membership_entity_commerce_prorate_calculate_prorating_discount',
    ),
    'arguments' => array(
      'line_item' => array(
        'label' => t('Line item'),
        'type' => 'commerce_line_item',
        'required' => 'true',
      ),
      'user' => array(
        'type' => 'user',
        'label' => t('User'),
      ),
      'discount_name' => array(
        'label' => t('Discount name'),
        'type' => 'token',
        'options list' => 'commerce_discount_entity_list',
      ),
    ),
  );
  $actions['membership_entity_commerce_prorate_cancel_old_membership'] = array(
    'label' => t('Cancel old membership'),
    'group' => 'Prorate Actions',
    'callbacks' => array(
      'execute' => 'membership_entity_commerce_prorate_cancel_obsolete_membership',
    ),
    'arguments' => array(
      'user' => array(
        'label' => t('User'),
        'type' => 'user',
        'required' => 'true',
      ),
    ),
  );
  return $actions;
}

/**
 * Condition membership_entity_commerce_prorate_condition_current_membership.
 */
function membership_entity_commerce_prorate_condition_current_membership($user) {
  $user_wrapper = entity_metadata_wrapper('user', $user);
  $uid = $user_wrapper->uid->value();

  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', 'membership_entity')
    ->propertyCondition('uid', $uid, '=')
    ->propertyCondition('status', 1, '=');
  $result = $query->execute();

  return (array_key_exists('membership_entity', $result));
}

/**
 * Rules action callback.
 *
 * Calculate prorating discount percentage based on amount of elapsed time for
 * active membership.
 */
function membership_entity_commerce_prorate_calculate_prorating_discount($line_item, $user, $discount_name) {
  $line_item_wrapper = entity_metadata_wrapper('commerce_line_item', $line_item);
  $user_wrapper = entity_metadata_wrapper('user', $user);
  $uid = $user_wrapper->uid->value();

  // Get Membership object.
  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', 'membership_entity')
    ->propertyCondition('uid', $uid, '=')
    ->propertyCondition('status', 1, '=');
  $result = $query->execute();

  $current_membership = array_shift(array_values($result));
  $current_membership = array_shift(array_values($current_membership));
  $mid = array();
  $mid[] = $current_membership->mid;
  $mid_int = intval($mid[0]);
  $current_membership_loaded = entity_load('membership_entity', $mid);
  $current_membership_obj = array_shift(array_values($current_membership_loaded));

  $current_time = new DateTime(NULL, new DateTimeZone(date_default_timezone_get()));
  $current_time_string = $current_time->format('Y-m-d H:i:s');
  $current_timestamp = $current_time->getTimestamp();

  // Get current Membership term.
  $term_query = new EntityFieldQuery();
  $term_query->entityCondition('entity_type', 'membership_entity_term')
    ->propertyCondition('mid', $mid_int, '=')
    ->propertyCondition('start', $current_time_string, '<=')
    ->propertyCondition('end', $current_time_string, '>');
  $term_result = $term_query->execute();

  $current_term = array_shift(array_values($term_result));
  $current_term = array_shift(array_values($current_term));
  $tid = array();
  $tid[] = $current_term->id;
  $current_term_loaded = entity_load('membership_entity_term', $tid);
  $current_term_obj = array_shift(array_values($current_term_loaded));

  // Calculate percentage of membership that has passed.
  $term_start = $current_term_obj->start;
  $term_end = $current_term_obj->end;
  $term_duration = strtotime($term_end) - strtotime($term_start);
  $elapsed = strtotime($current_time_string) - strtotime($term_start);
  $percent_elapsed = $elapsed / $term_duration;

  // Get base cost of original membership type to calculate dollar amount of
  // discount.
  $orig_prod_type = $current_membership_obj->type;
  $product_query = new EntityFieldQuery();
  $product_query->entityCondition('entity_type', 'commerce_product')
    ->propertyCondition('type', 'membership_entity_product', '=')
    ->propertyCondition('data', 'membership_entity_commerce', 'CONTAINS');
  $product_result = $product_query->execute();
  $orig_product = NULL;

  foreach ($product_result['commerce_product'] as $key => $product) {
    $product_loaded = commerce_product_load($product->product_id);
    $product_membership_type = $product_loaded->data['membership_entity_commerce']['type'];
    if ($product_membership_type == $orig_prod_type) {
      $orig_product = $product_loaded;
      break;
    }
    else {
      unset($product_result['commerce_product'][$key]);
    }
  }

  $orig_prod_wrapper = entity_metadata_wrapper('commerce_product', $orig_product);
  $unit_price = $orig_prod_wrapper->commerce_price->value();

  $discount_amount = array(
    'amount' => intval($unit_price['amount']) * $percent_elapsed * -1,
    'currency_code' => $unit_price['currency_code'],
  );

  commerce_discount_add_price_component($line_item_wrapper, $discount_name, $discount_amount);
}

/**
 * Rules action callback.
 *
 * @param object $user
 *   The User object.
 *
 *   Remove old membership after switching memberships with prorating discount.
 */
function membership_entity_commerce_prorate_cancel_obsolete_membership($user) {
  $user_wrapper = entity_metadata_wrapper('user', $user);
  $uid = $user_wrapper->uid->value();

  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', 'membership_entity')
    ->propertyCondition('uid', $uid, '=')
    ->propertyCondition('status', 1, '=');
  $result = $query->execute();

  $results = $result['membership_entity'];
  $mids = array();

  foreach ($results as $memb) {
    $mids[] = $memb->mid;
  }

  $memberships = entity_load('membership_entity', $mids);
  $membership = array_shift(array_values($memberships));
  $membership->status = 2;
  $membership->save();
}

<?php

/**
 * @file
 * Prorate discount default rules.
 */

/**
 * Implements hook_default_rules_configuration().
 */
function membership_entity_commerce_prorate_default_rules_configuration() {
  $rules = array();

  $rule = rules_reaction_rule();

  $rule->label = t('Remove old membership after prorating');
  $rule->active = TRUE;

  $rule->event('commerce_payment_order_paid_in_full')
    ->condition('membership_entity_commerce_prorate_condition_current_membership', array('user:select' => 'site:current-user'))
    ->action('membership_entity_commerce_prorate_cancel_old_membership', array('user:select' => 'site:current-user'));

  $rules['rules_remove_old_membership_after_prorating'] = $rule;

  $rule = rules_reaction_rule();

  $rule->label = t('Prorating discount');
  $rule->active = TRUE;

  $rule->event('commerce_product_calculate_sell_price')
    ->condition('commerce_discount_compatibility_check', array(
      'commerce_order:select' => 'commerce-line-item:order',
      'commerce_discount' => 'discount_prorating_discount',
    ))
    ->condition('membership_entity_commerce_prorate_condition_current_membership', array('user:select' => 'site:current-user'))
    ->action('membership_entity_commerce_prorate_apply_prorating_discount', array(
      'line_item:select' => 'commerce-line-item',
      'user:select' => 'site:current-user',
      'discount_name' => 'discount_prorating_discount',
    ));

  $rules['commerce_discount_rule_discount_prorating_discount'] = $rule;

  return $rules;
}
